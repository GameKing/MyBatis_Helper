package cn.mybatishelper.entity;

import cn.mybatishelper.entity.database.DatabaseEntity;
import cn.mybatishelper.exception.MybatisHelperException;

/**
 * 定义文件生成基类
 * User: GameKing
 */
public interface CreateFile {
    /**
     * 根据数据库信息生成物理文件
     *
     * @param databaseEntity
     * @throws MybatisHelperException
     */
    public abstract void createFile(DatabaseEntity databaseEntity) throws MybatisHelperException;
}
