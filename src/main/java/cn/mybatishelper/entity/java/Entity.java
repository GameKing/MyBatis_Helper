package cn.mybatishelper.entity.java;

import cn.mybatishelper.entity.database.DatabaseEntity;
import cn.mybatishelper.exception.MybatisHelperException;

/**
 * 实体基础接口，定义各个实体共有，且必需实现的方法
 *
 * @author GameKing
 */
public interface Entity {
    /**
     * 将实体生成代码
     *
     * @return
     */
    public String toCodeString(DatabaseEntity databaseEntity) throws MybatisHelperException;
}
