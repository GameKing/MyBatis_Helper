package cn.mybatishelper.builder;

/**
 * 建造者接口
 * 要通过Director装配的必须参数的话，可实现此接口
 * User: GameKing
 */
public interface Builder {
    /**
     * 生成数据信息解析器
     */
    public void buildJdbcParser();

    /**
     * 根据参数生成由数据库表向mybatis转化所需中间类
     */
    public void buildTableToMybatisVos();

    /**
     * 生成或者装配根路径
     */
    public void buildRootFilePath();

    /**
     * 生成或装配mapper接口需要继承的路径
     */
    public void buildMapperExtendUrl();
}
