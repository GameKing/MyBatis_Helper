package cn.mybatishelper.common;

import java.util.HashMap;
import java.util.Map;

/**
 * 数据类型常量类
 *
 * @author GameKing
 */
public abstract class DataTypeConstants {
    /**
     * 存放jdbc和java对应类型的map
     */
    public static final Map<String, String> JDBC_JAVA_MATCH_TYPE = new HashMap<String, String>();
    /**
     * 存放Java类型与完整路径的对应关系
     */
    public static final Map<String, String> JAVA_PACKAGE_MATCH = new HashMap<String, String>();
    /**
     * 存放java类型与JDBC类型的对应关系
     */
    public static final Map<String, String> JAVA_JDBC_MATCH_TYPE = new HashMap<String, String>();

    static {
        /**
         * 建立jdbc与Java类型的对应关系
         * 为避免不同数据库间的默认大小写问题，此处key全部调整为小写
         */
        //字符类
        JDBC_JAVA_MATCH_TYPE.put("varchar2", "String");
        JDBC_JAVA_MATCH_TYPE.put("varchar", "String");
        JDBC_JAVA_MATCH_TYPE.put("text", "String");
        JDBC_JAVA_MATCH_TYPE.put("char", "String");
        JDBC_JAVA_MATCH_TYPE.put("blob", "String");

        //时间类
        JDBC_JAVA_MATCH_TYPE.put("date", "Date");
        JDBC_JAVA_MATCH_TYPE.put("timestamptz", "Date");
        JDBC_JAVA_MATCH_TYPE.put("timestamp", "Date");
        JDBC_JAVA_MATCH_TYPE.put("datetime", "Date");

        //数值类
        JDBC_JAVA_MATCH_TYPE.put("number", "Long");
        JDBC_JAVA_MATCH_TYPE.put("float", "Float");
        JDBC_JAVA_MATCH_TYPE.put("double", "Double");
        JDBC_JAVA_MATCH_TYPE.put("int", "Integer");
        JDBC_JAVA_MATCH_TYPE.put("int2", "Integer");
        JDBC_JAVA_MATCH_TYPE.put("int4", "Integer");
        JDBC_JAVA_MATCH_TYPE.put("int8", "Long");
        JDBC_JAVA_MATCH_TYPE.put("bigint", "Long");

        /**
         * 建立Java类型与完整路径的对应关系
         */
        JAVA_PACKAGE_MATCH.put("Date", "java.util.Date");

        /**
         * 建立java类型到mybatis支持的jdbc类型间的关联关系
         */
        JAVA_JDBC_MATCH_TYPE.put("String", "VARCHAR");
        JAVA_JDBC_MATCH_TYPE.put("Long", "BIGINT");
        JAVA_JDBC_MATCH_TYPE.put("Integer", "BIGINT");
        JAVA_JDBC_MATCH_TYPE.put("Date", "DATE");
        JAVA_JDBC_MATCH_TYPE.put("Float", "FLOAT");
        JAVA_JDBC_MATCH_TYPE.put("Double", "DOUBLE");
    }
}
