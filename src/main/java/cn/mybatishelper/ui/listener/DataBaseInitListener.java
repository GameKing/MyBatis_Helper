package cn.mybatishelper.ui.listener;

import cn.mybatishelper.common.BaseInfoConstants;
import cn.mybatishelper.common.JdbcUtil;
import cn.mybatishelper.ui.HandlerInitJFrame;
import cn.mybatishelper.ui.UIConstants;
import cn.mybatishelper.ui.uienum.DataBaseInitEnum;
import org.apache.commons.lang.StringUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * 数据库参数初始化确定按钮监听类
 * 负责对参数的校验和打开下一个页面
 * User: GameKing
 * Date: 15-8-21
 * Time: 上午10:52
 */
public class DataBaseInitListener implements ActionListener {
    private JPanel panel;

    public DataBaseInitListener(JPanel panel) {
        this.panel = panel;
    }

    /**
     * 数据库参数
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        Component[] components = panel.getComponents();
        for (Component component : components) {
            if (component instanceof JTextField) {
                JTextField jTextField = (JTextField) component;
                if (StringUtils.isBlank(jTextField.getText())) {
                    JOptionPane.showMessageDialog(null, DataBaseInitEnum.getNameByValue(jTextField.getName()) + " 不能为空", "提示", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                boolean result = setValue(jTextField.getName(), jTextField.getText());
                if (!result) {
                    JOptionPane.showMessageDialog(null, DataBaseInitEnum.getNameByValue(jTextField.getName()) + " 验证失败，格式异常", "提示", JOptionPane.ERROR_MESSAGE);
                    return;
                }
            }
            if (component instanceof JComboBox) {
                JComboBox jComboBox = (JComboBox) component;
                setValue(jComboBox.getName(), jComboBox.getSelectedItem().toString());
            }
        }
        if (JdbcUtil.validateDatabase(BaseInfoConstants.dataBaseAddr, BaseInfoConstants.dataBaseAccount, BaseInfoConstants.dataBasePassword)) {
            initDataBaseAndOpenNextWin();
        } else {
            JOptionPane.showMessageDialog(null, "数据库连接测试失败，请检查数据库参数", "错误", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * 初始化数据库表名和打开下一个窗口
     */
    private void initDataBaseAndOpenNextWin() {
        String[] tables = JdbcUtil.getTables(BaseInfoConstants.dataBaseAddr, BaseInfoConstants.dataBaseAccount, BaseInfoConstants.dataBasePassword, BaseInfoConstants.dataBaseSchema);
        UIConstants.dataBaseJFrame.setVisible(false);
        UIConstants.mybatisJFrame = new HandlerInitJFrame(tables);
    }

    /**
     * 将UI上的值设置进全局参数
     *
     * @param name
     * @param value
     * @return 设置的结果，成功true,失败false
     */
    private boolean setValue(String name, String value) {
        if (name.equals(DataBaseInitEnum.DATABASE_TYPE.getValue())) {
            BaseInfoConstants.dataBaseType = value;
        }
        if (name.equals(DataBaseInitEnum.DATABASE_ADDR.getValue())) {
            if (!value.contains(BaseInfoConstants.dataBaseType)) {
                return false;
            }
            BaseInfoConstants.dataBaseAddr = value;
        }
        if (name.equals(DataBaseInitEnum.DATABASE_ACCOUNT.getValue())) {
            BaseInfoConstants.dataBaseAccount = value;
        }
        if (name.equals(DataBaseInitEnum.DATABASE_PASSWORD.getValue())) {
            BaseInfoConstants.dataBasePassword = value;
        }
        if (name.equals(DataBaseInitEnum.DATABASE_SCHEMA.getValue())) {
            BaseInfoConstants.dataBaseSchema = value;
        }
        return true;
    }
}
