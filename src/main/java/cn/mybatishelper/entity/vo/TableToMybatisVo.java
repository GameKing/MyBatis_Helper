package cn.mybatishelper.entity.vo;

import java.io.Serializable;

/**
 * 用于存放表向mybatis转化过程中可能会用到的参数
 *
 * @author GameKing
 */
public class TableToMybatisVo implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 表名
     */
    private String tableName;
    /**
     * java类包路径
     */
    private String javaEntityPackageUrl;
    /**
     * xml等文件包路径
     */
    private String xmlEntityPackageUrl;
    /**
     * 要生成的类名，放空则自动转换
     */
    private String objectName;

    public TableToMybatisVo() {
    }

    public TableToMybatisVo(String tableName, String javaEntityPackageUrl, String xmlEntityPackageUrl, String objectName) {
        this.tableName = tableName;
        this.javaEntityPackageUrl = javaEntityPackageUrl;
        this.xmlEntityPackageUrl = xmlEntityPackageUrl;
        this.objectName = objectName;
    }

    //-----------------------------------get/set----------------------------------------------------

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getJavaEntityPackageUrl() {
        return javaEntityPackageUrl;
    }

    public void setJavaEntityPackageUrl(String javaEntityPackageUrl) {
        this.javaEntityPackageUrl = javaEntityPackageUrl;
    }

    public String getXmlEntityPackageUrl() {
        return xmlEntityPackageUrl;
    }

    public void setXmlEntityPackageUrl(String xmlEntityPackageUrl) {
        this.xmlEntityPackageUrl = xmlEntityPackageUrl;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }
}
