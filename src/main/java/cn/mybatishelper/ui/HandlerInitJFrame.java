package cn.mybatishelper.ui;

import cn.mybatishelper.common.BaseInfoConstants;
import cn.mybatishelper.ui.listener.MyBatisInfoListener;
import cn.mybatishelper.ui.uienum.MybatiesInfoEnum;

import javax.swing.*;
import java.awt.*;

/**
 * 文件生成参数初始化窗口
 * User: GameKing
 * Date: 15-8-21
 * Time: 上午10:38
 */
public class HandlerInitJFrame extends JFrame {
    private String[] tables;
    private JPanel panel;
    private JScrollPane jScrollPane;
    private JList jList;

    public HandlerInitJFrame(String[] tables) {
        this.tables = tables;
        initGUI();
        setTitle("MyBaties_Helper");
        setResizable(false);
        setVisible(true);
        setSize(900, 600);
        setLocationRelativeTo(null);
        getContentPane().setLayout(null);
        getContentPane().add(panel);
        getContentPane().add(jScrollPane);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    private void initGUI() {
        initJScrollPanel();

        initPanel();
    }

    private void initJScrollPanel() {
        jList = new JList(tables);
        jList.setBorder(BorderFactory.createEtchedBorder());
        jScrollPane = new JScrollPane(jList);
        jScrollPane.setBounds(10, 10, 300, 500);
        jScrollPane.setBorder(BorderFactory.createTitledBorder("TABLES"));
    }

    /**
     * 初始化布局
     */
    private void initPanel() {
        panel = new JPanel();
        panel.setBounds(320, 10, 560, 500);
        panel.setBorder(BorderFactory.createTitledBorder("基础信息"));
        panel.setLayout(null);

        panel.add(createLable(MybatiesInfoEnum.ROOT_DIR.getName() + ":", 30, 60, 130, 15));
        panel.add(createLable(MybatiesInfoEnum.MAPPER_PACKAGE.getName() + ":", 30, 94, 130, 15));
        panel.add(createLable(MybatiesInfoEnum.PERSISTENCE_PACKAGE.getName() + ":", 30, 130, 130, 15));
        panel.add(createLable(MybatiesInfoEnum.TABLE_NAME_REMOVE.getName() + ":", 30, 164, 130, 15));
        panel.add(createLable(MybatiesInfoEnum.TABLE_NAME_SEPARATE.getName() + ":", 30, 198, 130, 15));
        panel.add(createLable(MybatiesInfoEnum.MAPPER_EXTEND_URL.getName() + ":", 30, 232, 130, 15));

        panel.add(createTextField(MybatiesInfoEnum.ROOT_DIR.getValue(), System.getProperty("user.dir"), 170, 52, 260, 30));
        panel.add(createTextField(MybatiesInfoEnum.MAPPER_PACKAGE.getValue(), BaseInfoConstants.mapperPackage, 170, 86, 260, 30));
        panel.add(createTextField(MybatiesInfoEnum.PERSISTENCE_PACKAGE.getValue(), BaseInfoConstants.poPackage, 170, 122, 260, 30));
        panel.add(createTextField(MybatiesInfoEnum.TABLE_NAME_REMOVE.getValue(), BaseInfoConstants.tableNameRemove, 170, 156, 260, 30));
        panel.add(createTextField(MybatiesInfoEnum.TABLE_NAME_SEPARATE.getValue(), BaseInfoConstants.tableNameSeparate, 170, 190, 260, 30));
        panel.add(createTextField(MybatiesInfoEnum.MAPPER_EXTEND_URL.getValue(), BaseInfoConstants.mapperExtendJavaUrl, 170, 224, 260, 30));
        panel.add(createJButton("确定", 190, 264, 150, 30));

        panel.add(createLable("ps : 根路径为绝对路径，文件的生成将以根路径为基础，如：E:\\mybatis_Helper", 30, 340, 450, 15, SystemColor.red, SwingConstants.LEFT));
        panel.add(createLable("ps : mapper-package层级为数据库DAO的存放路径，如：com.cn.mapper", 30, 360, 450, 15, SystemColor.red, SwingConstants.LEFT));
        panel.add(createLable("ps : po-package层级为持久化实体类的存放路径,如:com.cn.po", 30, 380, 450, 15, SystemColor.red, SwingConstants.LEFT));
        panel.add(createLable("ps : 表名舍弃字段, 例：T_USER_INFO, 舍弃：T，获得：_USER_INFO", 30, 400, 450, 15, SystemColor.red, SwingConstants.LEFT));
        panel.add(createLable("ps : 表名分隔字段，例: USER_INFO，分隔: _，获得: UserInfo", 30, 420, 450, 15, SystemColor.red, SwingConstants.LEFT));
        panel.add(createLable("ps : mapper接口父类，可为空，如果不为空的话，生成的mapper接口将继承该类", 30, 440, 450, 15, SystemColor.red, SwingConstants.LEFT));
        panel.add(createLable("ps : mapper继承父类时，生成的mapper将不会在接口内部声明增删改查，请在父类中定义", 30, 460, 500, 15, SystemColor.red, SwingConstants.LEFT));
    }

    /**
     * 可以指定字体颜色和对齐方式的JLABEL
     */
    private JLabel createLable(String text, int x, int y, int width, int height, Color color, int horizontalAlignment) {
        JLabel jLabel = new JLabel(text, horizontalAlignment);
        jLabel.setBounds(x, y, width, height);
        if (color != null) {
            jLabel.setForeground(color);
        }
        return jLabel;
    }

    private JLabel createLable(String text, int x, int y, int width, int height) {
        JLabel jLabel = new JLabel(text, SwingConstants.RIGHT);
        jLabel.setBounds(x, y, width, height);
        return jLabel;
    }

    private JTextField createTextField(String name, String text, int x, int y, int width, int height) {
        JTextField textField = new JTextField();
        textField.setText(text);
        textField.setName(name);
        textField.setBounds(x, y, width, height);
        return textField;
    }

    private JButton createJButton(String text, int x, int y, int width, int height) {
        JButton button = new JButton();
        button.setText(text);
        button.setBounds(x, y, width, height);
        button.addActionListener(new MyBatisInfoListener(jList, panel));
        return button;
    }
}
