package cn.mybatishelper.ui;

import cn.mybatishelper.common.BaseInfoConstants;
import cn.mybatishelper.enums.DataBaseTypeEnum;
import cn.mybatishelper.ui.listener.DataBaseInitListener;
import cn.mybatishelper.ui.uienum.DataBaseInitEnum;
import org.apache.commons.lang.StringUtils;

import javax.swing.*;

/**
 * 数据库初始化窗口
 * User: GameKing
 * Date: 15-8-21
 * Time: 上午10:38
 */
public class DataBaseInitJFrame extends JFrame {
    private JPanel panel;

    public DataBaseInitJFrame() {
        initGUI();
        setTitle("MyBaties_Helper");
        setResizable(false);
        setVisible(true);
        setSize(600, 600);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    private void initGUI() {
        panel = new JPanel();
        getContentPane().setLayout(null);
        getContentPane().add(panel);
        panel.setBounds(41, 34, 500, 500);
        panel.setBorder(BorderFactory.createTitledBorder("初始化数据库信息"));
        panel.setLayout(null);

        panel.add(createLable(DataBaseInitEnum.DATABASE_TYPE.getName() + ":", 44, 60, 69, 15));
        panel.add(createLable(DataBaseInitEnum.DATABASE_ADDR.getName() + ":", 44, 94, 69, 15));
        panel.add(createLable(DataBaseInitEnum.DATABASE_ACCOUNT.getName() + ":", 44, 130, 69, 15));
        panel.add(createLable(DataBaseInitEnum.DATABASE_PASSWORD.getName() + ":", 44, 164, 69, 15));
        panel.add(createLable(DataBaseInitEnum.DATABASE_SCHEMA.getName() + ":", 44, 196, 69, 15));

        panel.add(createJComboBox(DataBaseInitEnum.DATABASE_TYPE.getValue(), new String[]{DataBaseTypeEnum.MYSQL.getDataBaseType(), DataBaseTypeEnum.ORACLE.getDataBaseType(),
                DataBaseTypeEnum.POSTGRESQL.getDataBaseType()}, 119, 52, 133, 30));
        panel.add(createTextField(DataBaseInitEnum.DATABASE_ADDR.getValue(), BaseInfoConstants.dataBaseAddr, 119, 86, 300, 30));
        panel.add(createTextField(DataBaseInitEnum.DATABASE_ACCOUNT.getValue(), BaseInfoConstants.dataBaseAccount, 119, 122, 133, 30));
        panel.add(createTextField(DataBaseInitEnum.DATABASE_PASSWORD.getValue(), BaseInfoConstants.dataBasePassword, 119, 156, 133, 30));
        panel.add(createTextField(DataBaseInitEnum.DATABASE_SCHEMA.getValue(), BaseInfoConstants.dataBaseSchema, 119, 190, 133, 30));
        panel.add(createJButton("确定", 150, 254, 200, 30));
    }

    private JLabel createLable(String text, int x, int y, int width, int height) {
        JLabel jLabel = new JLabel(text);
        jLabel.setBounds(x, y, width, height);
        return jLabel;
    }

    private JTextField createTextField(String name, String text, int x, int y, int width, int height) {
        JTextField textField = new JTextField();
        textField.setText(text);
        textField.setName(name);
        textField.setBounds(x, y, width, height);
        return textField;
    }

    private JComboBox createJComboBox(String name, String[] items, int x, int y, int width, int height) {
        JComboBox<String> jComboBox = new JComboBox<String>();
        jComboBox.setName(name);
        jComboBox.setBounds(x, y, width, height);
        for (String item : items) {
            jComboBox.addItem(item);
        }
        if (StringUtils.isNotBlank(BaseInfoConstants.dataBaseType)) {
            jComboBox.setSelectedItem(BaseInfoConstants.dataBaseType);
        }
        return jComboBox;
    }

    private JButton createJButton(String text, int x, int y, int width, int height) {
        JButton button = new JButton();
        button.setText(text);
        button.setBounds(x, y, width, height);
        button.addActionListener(new DataBaseInitListener(panel));
        return button;
    }
}
