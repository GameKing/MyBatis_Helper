package cn.mybatishelper.ui.uienum;

import org.apache.commons.lang.StringUtils;

/**
 * 数据库参数显示名称和实际值对照枚举
 * User: GameKing
 * Date: 15-8-21
 * Time: 上午10:57
 */
public enum MybatiesInfoEnum {
    ROOT_DIR("根路径", "root_dir"),
    MAPPER_PACKAGE("mapper-package层级", "mapper_package"),
    PERSISTENCE_PACKAGE("po-package层级", "persistence_package"),
    TABLE_NAME_REMOVE("表名舍弃字段", "table_name_remove"),
    TABLE_NAME_SEPARATE("表名分隔字段", "table_name_separate"),
    MAPPER_EXTEND_URL("mapper接口父类路径", "mapper_extend_url");

    private String name;
    private String value;

    private MybatiesInfoEnum(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public static String getNameByValue(String value) {
        if (StringUtils.isBlank(value)) {
            return null;
        }
        for (MybatiesInfoEnum initEnum : MybatiesInfoEnum.values()) {
            if (initEnum.getValue().equals(value)) {
                return initEnum.getName();
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
