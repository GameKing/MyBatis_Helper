package cn.mybatishelper.enums;

/**
 * 数据库类型枚举
 * User: GameKing
 */
public enum DataBaseTypeEnum {
    MYSQL("MYSQL数据库", "mysql"),
    POSTGRESQL("postgresql数据库", "postgresql"),
    ORACLE("ORACLE数据库", "oracle");

    private String dataBaseName;
    private String dataBaseType;

    DataBaseTypeEnum(String dataBaseName, String dataBaseType) {
        this.dataBaseName = dataBaseName;
        this.dataBaseType = dataBaseType;
    }

    public String getDataBaseName() {
        return dataBaseName;
    }

    public void setDataBaseName(String dataBaseName) {
        this.dataBaseName = dataBaseName;
    }

    public String getDataBaseType() {
        return dataBaseType;
    }

    public void setDataBaseType(String dataBaseType) {
        this.dataBaseType = dataBaseType;
    }
}
