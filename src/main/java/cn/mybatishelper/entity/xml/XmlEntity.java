package cn.mybatishelper.entity.xml;

import cn.mybatishelper.entity.database.DatabaseEntity;
import cn.mybatishelper.exception.MybatisHelperException;

/**
 * mybatis的xml文件生成接口
 * User: GameKing
 */
public interface XmlEntity {
    /**
     * 根据数据库表信息生成对应的xml文件内容
     *
     * @param databaseEntity
     * @return
     */
    public String getMybatisString(DatabaseEntity databaseEntity) throws MybatisHelperException;

    /**
     * 验证xml元素有效性
     *
     * @return
     */
    public boolean validateXmlElements();
}
