package cn.mybatishelper.parser.jdbc;

import cn.mybatishelper.common.JdbcUtil;
import cn.mybatishelper.entity.database.ColumnEntity;
import cn.mybatishelper.entity.database.DatabaseEntity;
import cn.mybatishelper.entity.vo.TableToMybatisVo;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.ResultSet;
import java.util.List;

/**
 * User: GameKing
 */
public abstract class AbstractJdbcParser implements JdbcParser {
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractJdbcParser.class);
    /**
     * 数据库地址*
     */
    protected String addressUrl;
    /**
     * 数据库帐号*
     */
    protected String userName;
    /**
     * 数据库密码*
     */
    protected String password;
    /**
     * 模式名称 *
     */
    protected String schemaName;

    protected AbstractJdbcParser() {
    }

    protected AbstractJdbcParser(String addressUrl, String password, String schemaName, String userName) {
        this.addressUrl = addressUrl;
        this.password = password;
        this.schemaName = schemaName;
        this.userName = userName;
    }

    @Override
    public boolean validateParser() {
        if (StringUtils.isBlank(addressUrl)) {
            LOGGER.error("数据库地址为空，有效性校验失败...");
            return false;
        }
        if (StringUtils.isBlank(userName)) {
            LOGGER.error("数据库用户名为空，有效性校验失败...");
            return false;
        }
        if (StringUtils.isBlank(password)) {
            LOGGER.error("数据库连接密码为空，有效性校验失败...");
            return false;
        }
        if (StringUtils.isBlank(schemaName)) {
            LOGGER.error("数据库模式为空，有效性校验失败...");
            return false;
        }
        return true;
    }

    /**
     * 从结果集中取得数据库生成数据库实体
     */
    protected DatabaseEntity getDatabaseEntityFromSet(TableToMybatisVo vo, ResultSet resultSet, ResultSet primarySet) {
        if (resultSet == null) {
            return null;
        }
        List<ColumnEntity> columnList = JdbcUtil.getColumnEntity(resultSet, primarySet);

        DatabaseEntity entity = new DatabaseEntity();
        entity.setTableName(vo.getTableName());
        entity.setColumnList(columnList);
        entity.setObjectName(vo.getObjectName());
        entity.setJavaPackageName(vo.getJavaEntityPackageUrl());
        entity.setXmlPackageName(vo.getXmlEntityPackageUrl());
        return entity;
    }

    public String getAddressUrl() {
        return addressUrl;
    }

    public void setAddressUrl(String addressUrl) {
        this.addressUrl = addressUrl;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }
}
