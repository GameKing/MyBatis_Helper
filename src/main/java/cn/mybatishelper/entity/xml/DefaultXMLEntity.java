package cn.mybatishelper.entity.xml;

import cn.mybatishelper.entity.database.DatabaseEntity;
import cn.mybatishelper.entity.mybatis.*;
import cn.mybatishelper.exception.MybatisHelperException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * 对databaseEntity实体进行处理，生成所需xml内容的解析器
 *
 * @author GameKing
 */
public class DefaultXMLEntity extends MybatisXmlEntity {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultXMLEntity.class);

    /**
     * 采用默认元素组合来生成DefaultXMLParser
     */
    public DefaultXMLEntity() {
        super();
        LOGGER.info("初始化MybatisXMLParser, 采用默认元素组合...");
        elements = new ArrayList<MybatisElement>();
        elements.add(new ResultMapElement());
        elements.add(new BaseColumnElement());
        elements.add(new InsertMybatisElement());
        elements.add(new UpdateMybatisElement());
        elements.add(new DeleteMybatisElement());
        elements.add(new SelectMybatisElement());
    }

    /**
     * 采用入参元素组合来生成解析器
     *
     * @param elementList
     */
    public DefaultXMLEntity(List<MybatisElement> elementList) {
        super(elementList);
        LOGGER.info("初始化MybatisXMLParser，元素组合为参数elementList...");
    }

    public DefaultXMLEntity(List<MybatisElement> elements, String rootFilePath) {
        super(elements, rootFilePath);
    }

    public DefaultXMLEntity(String rootFilePath) {
        super(rootFilePath);
        elements = new ArrayList<MybatisElement>();
        elements.add(new ResultMapElement());
        elements.add(new BaseColumnElement());
        elements.add(new InsertMybatisElement());
        elements.add(new UpdateMybatisElement());
        elements.add(new DeleteMybatisElement());
        elements.add(new SelectMybatisElement());
    }

    @Override
    public String getMybatisString(DatabaseEntity databaseEntity) throws MybatisHelperException {
        if (!validateXmlElements()) {
            throw new MybatisHelperException("无效的xml元素组合...");
        }
        if (!databaseEntity.validateAndCompleteEntity()) {
            throw new MybatisHelperException("无效的数据库实体...");
        }
        LOGGER.info("执行生成mybatis.xml内容页方法,当前处理表:" + databaseEntity.getTableName() + "...");
        StringBuilder result = new StringBuilder("");
        result.append(title);
        result.append("\n");
        result.append(docTitle);
        result.append("\n");
        result.append("<mapper namespace=\"" + databaseEntity.getXmlPackageName() + "." + databaseEntity.getObjectName() + "Mapper\">");
        for (MybatisElement element : elements) {
            element.setDatabaseEntity(databaseEntity);
            String resultStr = element.toXmlString();
            if (StringUtils.isNotBlank(resultStr)) {
                result.append("\n");
                result.append(element.toXmlString());
            }
        }
        result.append("\n");
        result.append("</mapper>");
        return result.toString();
    }
}
