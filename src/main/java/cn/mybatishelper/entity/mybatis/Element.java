package cn.mybatishelper.entity.mybatis;

/**
 * 元素基础接口，定义各个元素共有，且必需实现的方法
 *
 * @author GameKing
 */
public interface Element {
    /**
     * 将元素按xml映射语句的格式进行转换
     *
     * @return
     */
    public String toXmlString();

    /**
     * 将元素按java方法代码的格式进行转换
     *
     * @return
     */
    public String toCodeString();
}
