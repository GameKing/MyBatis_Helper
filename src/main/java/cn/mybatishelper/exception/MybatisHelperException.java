package cn.mybatishelper.exception;

/**
 * User: GameKing
 */
public class MybatisHelperException extends Exception{

    public MybatisHelperException() {
    }

    public MybatisHelperException(Throwable cause) {
        super(cause);
    }

    public MybatisHelperException(String message) {
        super(message);
    }

    public MybatisHelperException(String message, Throwable cause) {
        super(message, cause);
    }
}
