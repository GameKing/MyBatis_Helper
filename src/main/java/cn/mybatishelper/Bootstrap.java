package cn.mybatishelper;

import cn.mybatishelper.ui.MybatisHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 引导类
 * Created by yaoxh on 2016/9/23.
 */
public class Bootstrap {

    private static final Logger LOGGER = LoggerFactory.getLogger(Bootstrap.class);

    public static void main(String[] args) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Class.forName("org.postgresql.Driver");
            Class.forName("oracle.jdbc.OracleDriver");
            MybatisHelper.execute();
        } catch (Exception e) {
            LOGGER.error("初始化过程发生异常", e);
        }
    }
}
