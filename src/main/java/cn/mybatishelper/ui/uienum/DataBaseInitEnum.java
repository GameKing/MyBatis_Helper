package cn.mybatishelper.ui.uienum;

import org.apache.commons.lang.StringUtils;

/**
 * 数据库参数显示名称和实际值对照枚举
 * User: GameKing
 * Date: 15-8-21
 * Time: 上午10:57
 */
public enum DataBaseInitEnum {
    DATABASE_TYPE("数据库类型", "database_type"),
    DATABASE_ADDR("数据库地址", "database_addr"),
    DATABASE_ACCOUNT("数据库账号", "database_account"),
    DATABASE_PASSWORD("数据库密码", "database_password"),
    DATABASE_SCHEMA("数据库模式", "database_schema");

    private String name;
    private String value;

    private DataBaseInitEnum(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public static String getNameByValue(String value) {
        if (StringUtils.isBlank(value)) {
            return null;
        }
        for (DataBaseInitEnum initEnum : DataBaseInitEnum.values()) {
            if (initEnum.getValue().equals(value)) {
                return initEnum.getName();
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
