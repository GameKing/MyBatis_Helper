package cn.mybatishelper.ui;

import javax.swing.*;

/**
 * User: GameKing
 */
public class UIConstants {
    /**
     * 数据库信息窗体
     */
    public static JFrame dataBaseJFrame;
    /**
     * mybatis信息窗体
     */
    public static JFrame mybatisJFrame;
}
