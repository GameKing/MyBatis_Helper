package cn.mybatishelper.entity.java;

import cn.mybatishelper.entity.CreateFile;
import cn.mybatishelper.entity.database.DatabaseEntity;
import cn.mybatishelper.exception.MybatisHelperException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * java实体类，定义生成java类相关的通用属性和自检方法
 *
 * @author GameKing
 */
public abstract class JavaEntity implements Entity, CreateFile {
    private static final Logger LOGGER = LoggerFactory.getLogger(JavaEntity.class);
    /**
     * 文件根路径，用于实例生成文件时的路径拼装
     */
    protected String rootFilePath;

    public JavaEntity() {
    }

    protected JavaEntity(String rootFilePath) {
        this.rootFilePath = rootFilePath;
    }

    /**
     * 转为java语句的方法*
     */
    @Override
    public abstract String toCodeString(DatabaseEntity databaseEntity) throws MybatisHelperException;

    /**
     * 校验数据库实体是否有效*
     */
    public boolean validateDatabaseEntity(DatabaseEntity databaseEntity) {
        if (databaseEntity == null) {
            LOGGER.error("初始化Entity元素时，databaseEntity为null!");
            return false;
        }
        if (!databaseEntity.validateAndCompleteEntity()) {
            LOGGER.error("初始化Entity元素时，databaseEntity验证无效!");
            return false;
        }
        if (StringUtils.isBlank(rootFilePath)) {
            LOGGER.error("文件根路径不能为空");
            return false;
        }
        return true;
    }

    public String getRootFilePath() {
        return rootFilePath;
    }

    public void setRootFilePath(String rootFilePath) {
        this.rootFilePath = rootFilePath;
    }
}
