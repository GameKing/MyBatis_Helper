package cn.mybatishelper.ui;

import cn.mybatishelper.common.BaseInfoConstants;
import cn.mybatishelper.enums.DataBaseTypeEnum;

/**
 * User: GameKing
 * Date: 15-8-21
 * Time: 上午11:15
 */
public class MybatisHelper {

    public static void execute() {
        HandleSawtooth.setUI();
        UIConstants.dataBaseJFrame = new DataBaseInitJFrame();
    }

    public static final void main(String[] args) {
        BaseInfoConstants.dataBaseAddr = "jdbc:postgresql://127.0.0.1:5432/fanschina";
        BaseInfoConstants.dataBaseAccount = "fans";
        BaseInfoConstants.dataBasePassword = "fans";
        BaseInfoConstants.dataBaseSchema = "fans";
        BaseInfoConstants.dataBaseType = DataBaseTypeEnum.POSTGRESQL.getDataBaseType();
        BaseInfoConstants.mapperExtendJavaUrl = "com.oms.tms.mapper.BaseMapper";
        BaseInfoConstants.mapperPackage = "com.oms.tms.mapper";
        BaseInfoConstants.rootDir = "E:/";
        BaseInfoConstants.poPackage = "com.oms.tms.domain";
        BaseInfoConstants.tableNameSeparate = "_";
        BaseInfoConstants.tableNames = "t_fa_user";
        BaseInfoConstants.tableNameRemove = "t_fa_";

        MybatisHelper.execute();
    }
}
