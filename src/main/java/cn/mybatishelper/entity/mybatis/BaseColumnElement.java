package cn.mybatishelper.entity.mybatis;

import cn.mybatishelper.entity.database.ColumnEntity;
import cn.mybatishelper.entity.database.DatabaseEntity;

/**
 * mybatis的sql类型元素
 * 本例用于生成表单的列元素集合的映射语句
 *
 * @author GameKing
 */
public class BaseColumnElement extends MybatisElement {
    /**
     * 元素的标签，在此为sql类型
     */
    private static final String type = "sql";
    /**
     * mybatis元素的id属性
     */
    public static final String id = "base_column";

    public BaseColumnElement() {
        super("base_column", null, false);
    }

    public BaseColumnElement(String id, DatabaseEntity databaseEntity, boolean isMethod) {
        super(id, databaseEntity, isMethod);
    }

    @Override
    public String toXmlString() {
        if (!validateDatabaseEntity()) {
            return null;
        }

        StringBuilder xmlStrBuffer = new StringBuilder("\n");
        xmlStrBuffer.append("\t");
        xmlStrBuffer.append("<sql id=\'" + BaseColumnElement.id + "\'>");
        xmlStrBuffer.append("\n");
        xmlStrBuffer.append("\t\t");
        for (int i = 0; i < databaseEntity.getColumnList().size(); i++) {
            ColumnEntity column = databaseEntity.getColumnList().get(i);
            xmlStrBuffer.append(column.getColumnName());
            if (i < databaseEntity.getColumnList().size() - 1) {
                xmlStrBuffer.append(",");
            }
        }
        xmlStrBuffer.append("\n");
        xmlStrBuffer.append("\t");
        xmlStrBuffer.append("</sql>");

        return xmlStrBuffer.toString();
    }

    @Override
    public String toCodeString() {
        return null;
    }

    @Override
    public boolean validateElement() {
        return true;
    }

    /**
     * ************************ get/set方法 *********************************
     */

    public String getType() {
        return type;
    }
}
