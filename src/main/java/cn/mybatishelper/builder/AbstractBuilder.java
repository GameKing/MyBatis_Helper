package cn.mybatishelper.builder;

import cn.mybatishelper.common.BaseInfoConstants;
import cn.mybatishelper.common.JdbcUtil;
import cn.mybatishelper.entity.vo.TableToMybatisVo;
import cn.mybatishelper.enums.DataBaseTypeEnum;
import cn.mybatishelper.parser.jdbc.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * 建造者基类
 * Created by yaoxh on 2016/9/22.
 */
public abstract class AbstractBuilder implements Builder {

    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());
    /**
     * source folder 路径，生成文件使用
     */
    protected String rootFilePath;
    /**
     * 数据库解析器
     */
    protected AbstractJdbcParser jdbcParser;
    /**
     * mapper需要继承的父类的完整路径
     */
    protected String extendsClassUrl;
    /**
     * 数据库表和需要生成的业务类信息的集合列表
     */
    protected List<TableToMybatisVo> voList;

    public AbstractBuilder() {
    }

    public AbstractBuilder(String extendsClassUrl, AbstractJdbcParser jdbcParser, String rootFilePath, List<TableToMybatisVo> voList) {
        this.extendsClassUrl = extendsClassUrl;
        this.jdbcParser = jdbcParser;
        this.rootFilePath = rootFilePath;
        this.voList = voList;
    }

    @Override
    public void buildJdbcParser() {
        if (DataBaseTypeEnum.ORACLE.getDataBaseType().equals(BaseInfoConstants.dataBaseType)) {
            this.jdbcParser = new OracleJdbcParser();
        } else if (DataBaseTypeEnum.POSTGRESQL.getDataBaseType().equals(BaseInfoConstants.dataBaseType)) {
            this.jdbcParser = new PostgresqlJdbcParser();
        } else {
            this.jdbcParser = new MysqlJdbcParser();
        }
        this.jdbcParser.setAddressUrl(BaseInfoConstants.dataBaseAddr);
        this.jdbcParser.setUserName(BaseInfoConstants.dataBaseAccount);
        this.jdbcParser.setPassword(BaseInfoConstants.dataBasePassword);
        this.jdbcParser.setSchemaName(BaseInfoConstants.dataBaseSchema);
    }

    @Override
    public void buildTableToMybatisVos() {
        this.voList = JdbcUtil.createTableToMybatisFromBaseConstants();
    }

    @Override
    public void buildRootFilePath() {
        this.rootFilePath = BaseInfoConstants.rootDir;
    }

    @Override
    public void buildMapperExtendUrl() {
        this.extendsClassUrl = BaseInfoConstants.mapperExtendJavaUrl;
    }

    /**
     * ******************************get/set********************************************************
     */

    public String getRootFilePath() {
        return rootFilePath;
    }

    public void setRootFilePath(String rootFilePath) {
        this.rootFilePath = rootFilePath;
    }

    public String getExtendsClassUrl() {
        return extendsClassUrl;
    }

    public void setExtendsClassUrl(String extendsClassUrl) {
        this.extendsClassUrl = extendsClassUrl;
    }

    public JdbcParser getJdbcParser() {
        return jdbcParser;
    }

    public void setJdbcParser(AbstractJdbcParser jdbcParser) {
        this.jdbcParser = jdbcParser;
    }

    public List<TableToMybatisVo> getVoList() {
        return voList;
    }

    public void setVoList(List<TableToMybatisVo> voList) {
        this.voList = voList;
    }
}
