package cn.mybatishelper.parser.jdbc;

import cn.mybatishelper.entity.database.DatabaseEntity;
import cn.mybatishelper.entity.vo.TableToMybatisVo;

import java.util.List;

/**
 * User: GameKing
 * Date: 15-8-21
 * Time: 上午11:32
 */
public interface JdbcParser {
    /**
     * 获取数据库实体
     */
    public List<DatabaseEntity> getJdbcEntitys(List<TableToMybatisVo> vos);

    /**
     * 校验数据库解析器是否可用
     */
    public boolean validateParser();
}
