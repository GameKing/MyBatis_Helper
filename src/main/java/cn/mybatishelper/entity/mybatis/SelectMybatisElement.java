package cn.mybatishelper.entity.mybatis;

import cn.mybatishelper.entity.database.ColumnEntity;
import cn.mybatishelper.entity.database.DatabaseEntity;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * mybatis查询类型的元素，即<select id="xx"/>类型
 * 一般用于生成查询的映射语句
 *
 * @author GameKing
 */
public class SelectMybatisElement extends MybatisElement {
    private static final Logger LOGGER = LoggerFactory.getLogger(SelectMybatisElement.class);
    /**
     * 元素的标签，在此为select类型  *
     */
    private final String type = "select";

    /**
     * 返回类型，一般有两种值，1.resultMap 2.returnType *
     */
    private final String resultType = "resultMap";

    /**
     * 返回类型的值   *
     */
    private String resultValue;

    public SelectMybatisElement() {
        super("findById", null, true);
        this.resultValue = "BaseResultMap";
    }


    public SelectMybatisElement(String id, DatabaseEntity databaseEntity, boolean isMethod, String resultValue) {
        super(id, databaseEntity, isMethod);
        this.resultValue = resultValue;
    }


    @Override
    public String toXmlString() {
        if (!validateDatabaseEntity()) {
            return null;
        }
        if (!validateElement()) {
            return null;
        }

        StringBuilder stringBuilder = new StringBuilder("\n");
        stringBuilder.append("\t");
        stringBuilder.append("<select id=\'" + this.id + "\' " + this.resultType + "=\'" + this.resultValue + "\'>");
        stringBuilder.append("\n");
        stringBuilder.append("\t\t");
        stringBuilder.append("SELECT * ");
        stringBuilder.append("\n");
        stringBuilder.append("\t\t");
        stringBuilder.append("FROM " + databaseEntity.getTableName() + " ");
        stringBuilder.append("\n");
        stringBuilder.append("\t\t");
        stringBuilder.append("WHERE ");
        if (databaseEntity.getPrimaryKey() != null) {
            ColumnEntity column = databaseEntity.getPrimaryKey();
            stringBuilder.append(column.getColumnName() + " = #{id,jdbcType=DECIMAL}");
        }
        stringBuilder.append("\n");
        stringBuilder.append("\t");
        stringBuilder.append("</select>");

        return stringBuilder.toString();
    }

    @Override
    public String toCodeString() {
        if (!validateElement()) {
            return null;
        }

        StringBuilder xmlStrBuffer = new StringBuilder("\n\t");
        xmlStrBuffer.append("public " + databaseEntity.getObjectName() + " " + this.id + "(Long id);");
        xmlStrBuffer.append("\n");
        return xmlStrBuffer.toString();
    }

    @Override
    public boolean validateElement() {
        if (StringUtils.isEmpty(this.resultValue)) {
            LOGGER.error("初始化 " + databaseEntity.getTableName() + " 的select元素时，resultValue为null!");
            return false;
        }

        if (StringUtils.isEmpty(this.id)) {
            LOGGER.error("初始化Element元素时，id为null!");
            return false;
        }

        return true;
    }

    /**
     * ***************************get/set方法************************************
     */
    public String getResultType() {
        return resultType;
    }

    public String getResultValue() {
        return resultValue;
    }

    public void setResultValue(String resultValue) {
        this.resultValue = resultValue;
    }

    public String getType() {
        return type;
    }
}
