package cn.mybatishelper.builder;

import cn.mybatishelper.exception.MybatisHelperException;

/**
 * 处理程序基类
 *
 * @author GameKing
 */
public interface Handler {
    /**
     * 公共处理方法
     */
    public void handle() throws MybatisHelperException;

    /**
     * 公共的自检方法
     *
     * @return 是否有效
     */
    public boolean validate() throws MybatisHelperException;
}
