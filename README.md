MyBatis_Helper
===================
一款用于自动生成mybatis相关持久化类和接口的小框架，节省日常开发工作时间，目前支持oracle，postgresql和mysql的常用字段，有额外的需求可以提出 或者 自己添加...

附件中有个可执行的jar文件，下载后 直接双击 或通过 java -jar mybatis_helper.jar 命令启动即可...

通过代码的UI界面启动方式如下：

```
    /**
     * 正常启动方法
     */
    public static void main(String[] args) {
        MybatisHelper.execute();
    }
```

也可以参考src\test\java下的例子进行参数预设，不用每次启动都设置参数 或者 直接绕过UI界面...