package cn.mybatishelper.entity.database;

/**
 * 属性基类，用于定义数据库表单所拥有的属性及其对应名称
 *
 * @author GameKing
 */
public class ColumnEntity {
    /**
     * 数据库列名称
     */
    private String columnName;
    /**
     * 持久化后的变量名
     */
    private String propertyName;
    /**
     * 数据库中的数据类型
     */
    private String jdbcType;
    /**
     * 在java中对应的数据类型
     */
    private String javaType;
    /**
     * 是否为主键
     */
    private boolean isPrimaryKey;


    /**
     * *************************************get/set方法**********************************************
     */

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getJdbcType() {
        return jdbcType;
    }

    public void setJdbcType(String jdbcType) {
        this.jdbcType = jdbcType;
    }

    public String getJavaType() {
        return javaType;
    }

    public void setJavaType(String javaType) {
        this.javaType = javaType;
    }

    public boolean isPrimaryKey() {
        return isPrimaryKey;
    }

    public void setPrimaryKey(boolean isPrimaryKey) {
        this.isPrimaryKey = isPrimaryKey;
    }
}
