import cn.mybatishelper.ui.MybatisHelper;

/**
 * 正常启动方式
 * User: GameKing
 */
public class UseExample {

    /**
     * 正常启动方法
     */
    public static void main(String[] args) {
        MybatisHelper.execute();
    }
}
