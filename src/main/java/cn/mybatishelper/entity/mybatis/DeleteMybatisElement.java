package cn.mybatishelper.entity.mybatis;

import cn.mybatishelper.entity.database.ColumnEntity;
import cn.mybatishelper.entity.database.DatabaseEntity;

/**
 * 个人定制版删除元素，用于生成基础删除的xml映射语句
 *
 * @author GameKing
 */
public class DeleteMybatisElement extends MybatisElement {
    /**
     * 元素的标签，此处为update **
     */
    private static final String type = "delete";

    public DeleteMybatisElement() {
        super("delete", null, true);
    }

    public DeleteMybatisElement(String id, DatabaseEntity databaseEntity, boolean isMethod) {
        super(id, databaseEntity, isMethod);
    }

    @Override
    public String toXmlString() {
        if (!validateDatabaseEntity()) {
            return null;
        }
        if (!validateElement()) {
            return null;
        }
        if (databaseEntity.getPrimaryKey() == null) {
            return null;
        }

        StringBuilder xmlStrBuffer = new StringBuilder("\n");
        xmlStrBuffer.append("\t");
        xmlStrBuffer.append("<delete id='" + this.id + "'>");
        xmlStrBuffer.append("\n");
        xmlStrBuffer.append("\t\t");
        xmlStrBuffer.append("DELETE FROM " + databaseEntity.getTableName());
        xmlStrBuffer.append("\n");
        xmlStrBuffer.append("\t\t");

        ColumnEntity column = databaseEntity.getPrimaryKey();
        xmlStrBuffer.append("WHERE " + column.getColumnName() + " = #{" + column.getPropertyName() + ",jdbcType=" + column.getJdbcType() + "}");
        xmlStrBuffer.append("\n");
        xmlStrBuffer.append("\t");
        xmlStrBuffer.append("</delete>");

        return xmlStrBuffer.toString();
    }

    @Override
    public String toCodeString() {
        if (!validateElement()) {
            return null;
        }

        StringBuilder xmlStrBuffer = new StringBuilder("\n\t");
        xmlStrBuffer.append("public int " + this.id + "(Long id);");
        xmlStrBuffer.append("\n");
        return xmlStrBuffer.toString();
    }


    /**
     * *****************************get/set方法*****************************************
     */
    public String getType() {
        return type;
    }

}
