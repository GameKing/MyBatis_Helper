package cn.mybatishelper.entity.mybatis;

import cn.mybatishelper.entity.database.DatabaseEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * mybaitis元素基类，实现了Element接口
 * 定义了一系列mybatis元素共有的属性和方法
 *
 * @author GameKing
 */
public abstract class MybatisElement implements Element {
    private static final Logger LOGGER = LoggerFactory.getLogger(MybatisElement.class);
    /**
     * mybatis元素的id属性*
     */
    protected String id;
    /**
     * 用于判断该元素是基础组件还是方法*
     */
    protected boolean isMethod;

    /**
     * 数据库表对应的实体类，里面存储了对应的库表信息以及在持久化类中的对应名称*
     */
    protected DatabaseEntity databaseEntity;

    public MybatisElement() {
    }

    public MybatisElement(String id, DatabaseEntity databaseEntity, boolean isMethod) {
        this.id = id;
        this.databaseEntity = databaseEntity;
        this.isMethod = isMethod;
    }

    /**
     * 转为xml映射语句的方法*
     */
    @Override
    public abstract String toXmlString();

    /**
     * 校验当前元素是否有效*
     */
    public boolean validateElement() {
        if (this.id == null || "".equals(this.id)) {
            LOGGER.error("初始化Element元素时，id为null!");
            return false;
        }
        return true;
    }

    /**
     * 校验数据库实体是否有效*
     */
    public boolean validateDatabaseEntity() {
        if (this.databaseEntity == null) {
            LOGGER.error("初始化Element元素时，databaseEntity为null!");
            return false;
        }
        if (!this.databaseEntity.validateAndCompleteEntity()) {
            LOGGER.error("初始化Element元素时，databaseEntity验证无效!");
            return false;
        }
        return true;
    }

    /**
     * ************以下为get/set方法***************
     */
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DatabaseEntity getDatabaseEntity() {
        return databaseEntity;
    }

    public void setDatabaseEntity(DatabaseEntity databaseEntity) {
        this.databaseEntity = databaseEntity;
    }

    public boolean isMethod() {
        return isMethod;
    }

    public void setMethod(boolean isMethod) {
        this.isMethod = isMethod;
    }
}
