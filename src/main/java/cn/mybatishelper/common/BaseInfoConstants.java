package cn.mybatishelper.common;

/**
 * User: GameKing
 */
public class BaseInfoConstants {
    /**
     * 数据库类型
     */
    public static String dataBaseType;
    /**
     * 数据库地址
     */
    public static String dataBaseAddr;
    /**
     * 数据库账号
     */
    public static String dataBaseAccount;
    /**
     * 数据库密码
     */
    public static String dataBasePassword;
    /**
     * 数据库模式名称
     */
    public static String dataBaseSchema;
    /**
     * 根路径
     */
    public static String rootDir;
    /**
     * mapper文件路径
     */
    public static String mapperPackage;
    /**
     * mapper文件路径
     */
    public static String mapperExtendJavaUrl;
    /**
     * 持久化文件路径
     */
    public static String poPackage;
    /**
     * 要从表名中移除的字段
     */
    public static String tableNameRemove;
    /**
     * 要将表名分隔的符号
     */
    public static String tableNameSeparate;
    /**
     * 要处理的数据库表名,多个表名间以逗号分隔
     */
    public static String tableNames;
}
