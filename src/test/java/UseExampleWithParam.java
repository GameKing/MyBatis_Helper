import cn.mybatishelper.builder.DefaultHandler;
import cn.mybatishelper.common.BaseInfoConstants;
import cn.mybatishelper.enums.DataBaseTypeEnum;
import cn.mybatishelper.exception.MybatisHelperException;
import cn.mybatishelper.ui.MybatisHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 定制参数版，提前录入参数，不需每次启动都重新一堆烦人的字符
 * User: GameKing
 */
public class UseExampleWithParam {

    private static final Logger LOGGER = LoggerFactory.getLogger(UseExampleWithParam.class);

    /**
     * 生成方式
     *
     * @param args
     */
    public static void main(String[] args) {
//        executeFromCode();
        executeFromWindow();
    }

    /**
     * 直接从代码生成
     */
    public static void executeFromCode() {
        try {
            initParam();
            new DefaultHandler().handle();
        } catch (MybatisHelperException e) {
            LOGGER.error("从代码生成文件发生异常", e);
        }
    }

    /**
     * 启动窗体来生成
     */
    public static void executeFromWindow() {
        initParam();
        MybatisHelper.execute();
    }

    public static void initParam() {
        BaseInfoConstants.dataBaseAddr = "jdbc:mysql://127.0.0.1:3306/vcloud";
        BaseInfoConstants.dataBaseAccount = "root";
        BaseInfoConstants.dataBasePassword = "123456";
        BaseInfoConstants.dataBaseSchema = "vcloud";
        BaseInfoConstants.dataBaseType = DataBaseTypeEnum.POSTGRESQL.getDataBaseType();
        BaseInfoConstants.mapperExtendJavaUrl = "com.oms.tms.mapper.BaseMapper";
        BaseInfoConstants.mapperPackage = "com.oms.tms.mapper";
        BaseInfoConstants.rootDir = "E:/";
        BaseInfoConstants.poPackage = "com.oms.tms.domain";
        BaseInfoConstants.tableNameSeparate = "_";
        BaseInfoConstants.tableNames = "t_fa_user";
        BaseInfoConstants.tableNameRemove = "t_fa_";
    }
}
