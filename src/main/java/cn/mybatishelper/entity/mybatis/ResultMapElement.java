package cn.mybatishelper.entity.mybatis;

import cn.mybatishelper.entity.database.ColumnEntity;
import cn.mybatishelper.entity.database.DatabaseEntity;

/**
 * 结果类型元素，即<resultMap id="xxx"/>类型元素 
 * @author yao
 *
 */
public class ResultMapElement extends MybatisElement {
	/** 元素的标签，在此为resultMap类型 **/
	private static final String type="resultMap";
	
	public ResultMapElement(){
		super("BaseResultMap", null, false);
	}

	public ResultMapElement(String id, DatabaseEntity databaseEntity, boolean isMethod) {
		super(id, databaseEntity, isMethod);
	}

	@Override
	public String toXmlString() {
		if(!validateDatabaseEntity()){
			return null;
		}
		
		if(!validateElement()){
			return null;
		}
		
		StringBuilder xmlStrBuffer = new StringBuilder("\n");
		xmlStrBuffer.append("\t");
		xmlStrBuffer.append("<resultMap id='").append(this.id).append("' type='").append(databaseEntity.getJavaPackageName()).append(".").append(databaseEntity.getObjectName()).append("'>");
		for(ColumnEntity column : databaseEntity.getColumnList()){
			xmlStrBuffer.append("\n");
			xmlStrBuffer.append("\t\t");
			xmlStrBuffer.append("<result column='").append(column.getColumnName()).append("' property='").append(column.getPropertyName()).append("'/>");
		}
		xmlStrBuffer.append("\n");
		xmlStrBuffer.append("\t");
		xmlStrBuffer.append("</resultMap>");

		return xmlStrBuffer.toString();
	}
	
	public String toCodeString() {
		if (!validateDatabaseEntity()) {
			return null;
		}
		
		StringBuilder propertyStr = new StringBuilder();
		StringBuilder getSetStr = new StringBuilder();
		for(ColumnEntity column : databaseEntity.getColumnList()){
			propertyStr.append("\n");
			propertyStr.append("\t");
			propertyStr.append("private ").append(column.getJavaType()).append(" ").append(column.getPropertyName()).append(";");
			
			String property = column.getPropertyName();
			property = property.replaceFirst(property.substring(0, 1),property.substring(0, 1).toUpperCase());
			getSetStr.append("\n");
			getSetStr.append("\t");
			getSetStr.append("public ").append(column.getJavaType()).append(" get").append(property).append("() {");
			getSetStr.append("\n\t\t");
			getSetStr.append("return ").append(column.getPropertyName()).append(";");
			getSetStr.append("\n\t");
			getSetStr.append("}");
			getSetStr.append("\n");
			getSetStr.append("\t");
			getSetStr.append("public void set").append(property).append("(").append(column.getJavaType()).append(" ").append(column.getPropertyName()).append(") {");
			getSetStr.append("\n\t\t");
			getSetStr.append("this.").append(column.getPropertyName()).append(" = ").append(column.getPropertyName()).append(";");
			getSetStr.append("\n\t");
			getSetStr.append("}");
		}

		return propertyStr.toString()+"\n\n"+getSetStr.toString();
	}

	
	/******************************get/set方法*************************************/
	public String getType() {
		return type;
	}

}
