package cn.mybatishelper.entity.mybatis;

import cn.mybatishelper.entity.database.ColumnEntity;
import cn.mybatishelper.entity.database.DatabaseEntity;

/**
 * mybatis的update元素，即<update id="xxx">类型
 *
 * @author GameKing
 */
public class UpdateMybatisElement extends MybatisElement {
    /**
     * 元素的标签，在此为update类型  *
     */
    private static final String type = "update";

    public UpdateMybatisElement() {
        super("update", null, true);
    }

    public UpdateMybatisElement(String id, DatabaseEntity databaseEntity, boolean isMethod) {
        super(id, databaseEntity, isMethod);
    }

    @Override
    public String toXmlString() {
        if (!databaseEntity.validateAndCompleteEntity()) {
            return null;
        }
        if (!validateElement()) {
            return null;
        }
        if (databaseEntity.getPrimaryKey() == null) {
            return null;
        }
        StringBuilder stringBuilder = new StringBuilder("\n");
        stringBuilder.append("\t");
        stringBuilder.append("<update id=\"").append(super.id).append("\">");
        stringBuilder.append("\n");
        stringBuilder.append("\t\t");
        stringBuilder.append("UPDATE	").append(databaseEntity.getTableName());
        stringBuilder.append("\n");
        stringBuilder.append("\t\t");
        stringBuilder.append("<set>");
        for (int i = 0; i < databaseEntity.getColumnList().size(); i++) {
            ColumnEntity column = databaseEntity.getColumnList().get(i);
            if (column.isPrimaryKey()) {
                continue;
            }
            stringBuilder.append("\n");
            stringBuilder.append("\t\t\t");
            stringBuilder.append("<if test='@Ognl@isNotEmpty(").append(column.getPropertyName()).append(")'>");
            stringBuilder.append("\n");
            stringBuilder.append("\t\t\t\t");
            stringBuilder.append(column.getColumnName()).append(" = #{").append(column.getPropertyName()).append(",jdbcType=").append(column.getJdbcType()).append("}");
            if (i < databaseEntity.getColumnList().size() - 1) {
                stringBuilder.append(",");
            }
            stringBuilder.append("\n");
            stringBuilder.append("\t\t\t");
            stringBuilder.append("</if>");
        }
        stringBuilder.append("\n");
        stringBuilder.append("\t\t");
        stringBuilder.append("</set>");

        ColumnEntity column = databaseEntity.getPrimaryKey();
        stringBuilder.append("\n");
        stringBuilder.append("\t\t");
        stringBuilder.append("WHERE ").append(column.getColumnName()).append(" = #{").append(column.getPropertyName()).append(",jdbcType=").append(column.getJdbcType()).append("}");
        stringBuilder.append("\n");
        stringBuilder.append("\t");
        stringBuilder.append("</update>");

        return stringBuilder.toString();
    }

    @Override
    public String toCodeString() {
        if (!validateElement()) {
            return null;
        }

        StringBuilder stringBuilder = new StringBuilder("\n\t");
        stringBuilder.append("public int ").append(this.id).append("(").append(databaseEntity.getObjectName()).append(" entity);");
        stringBuilder.append("\n");
        return stringBuilder.toString();
    }

    /**
     * ************************get/set方法******************************
     */
    public String getType() {
        return type;
    }

}
